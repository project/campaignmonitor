# Campaign Monitor

## Installation

Install the module as usual

From the drupal root directory of your install:

```
composer config repositories.drupal composer https://packages.drupal.org/8
composer require drupal/campaignmonitor

```

This will install the required library in the /vendor directory
