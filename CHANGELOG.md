# Changelog

## 8.x-2.3

* [Update license and add configure link](https://www.drupal.org/project/campaignmonitor/issues/3251602)
* [Settings page issues](https://www.drupal.org/project/campaignmonitor/issues/3183095)
* [Error: Call to undefined method Drupal\Core\Cache\DatabaseBackend::getBins() in Drupal\campaignmonitor\CampaignMonitorManager->clearCache()](https://www.drupal.org/project/campaignmonitor/issues/3183107)
* [Notice: Trying to access array offset on value of type bool in Drupal\campaignmonitor\CampaignMonitorSubscriptionManager->singleSubscribeForm() (line 139 of modules/contrib/campaignmonitor/src/CampaignMonitorSubscriptionManager.php). Drupal\campaignmonito](https://www.drupal.org/project/campaignmonitor/issues/3240656)
* [BlockPluginTrait->baseConfigurationDetails has no pluginId](https://www.drupal.org/project/campaignmonitor/issues/3221599)
* [Error: Call to a member function __toString() on string](https://www.drupal.org/project/campaignmonitor/issues/3183234)

## 8.x-2.0

* [Errors when submitting the value](https://www.drupal.org/project/campaignmonitor/issues/3149577)
* [Incorrect service arguments](https://www.drupal.org/project/campaignmonitor/issues/3162970)
* [$this->t() should be used instead of t() for Drupal 8 version](https://www.drupal.org/project/campaignmonitor/issues/3157643)
* [Error: Uncaught exception 'Error' with message 'Call to undefined method Drupal\campaignmonitor\CampaignMonitorManager::logger()' in campaignmonitor/src/CampaignMonitorManager.php:122](https://www.drupal.org/project/campaignmonitor/issues/3153093)
* [List disable/enable redundant?](https://www.drupal.org/project/campaignmonitor/issues/2911962)
* [Drupal 9 compatibility](https://www.drupal.org/project/campaignmonitor/issues/3149575)
* [PHP notice due to undefined variable](https://www.drupal.org/project/campaignmonitor/issues/3164798)
* [Name is not recorded on subscription](https://www.drupal.org/project/campaignmonitor/issues/3149578)

q## 8.x-1.0-beta7

* [Api key is truncated](https://www.drupal.org/project/campaignmonitor/issues/2995781)
* [Drupal crashes if clientID is not set](https://www.drupal.org/project/campaignmonitor/issues/2997059)
* [getCacheTimeout() returns null](https://www.drupal.org/project/campaignmonitor/issues/2954434)
* [Placing block crashes the website if `Campaign Monitor` module turned on but `Campaign Monitor Campaign` isn't](https://www.drupal.org/project/campaignmonitor/issues/3008157)

## 8.x-1.0-beta6

* Fix error: https://www.drupal.org/project/campaignmonitor/issues/2940318
* Remove states in registration: https://www.drupal.org/project/campaignmonitor/issues/2941242
* Fix invalid function call: https://www.drupal.org/project/campaignmonitor/issues/2941672
* Remove unwarranted security alert: https://www.drupal.org/project/campaignmonitor/issues/2941648
Improve usability: https://www.drupal.org/project/campaignmonitor/issues/2941242

## 8.x-1.0-beta4

* Check plain fixes: https://www.drupal.org/node/2911941
* Fix references to missing function: https://www.drupal.org/node/2883787

## 8.x-1.0-beta3

* Fix composer.json: https://www.drupal.org/node/2887580
* Codesniffer

## 8.x-1.0-beta3
* Fix still required for https://www.drupal.org/node/2883787

## 8.x-1.0-beta2

* [Fix twig template](https://www.drupal.org/node/2888014)
* [Fix composer.json error](https://www.drupal.org/node/2887580)
* [Remove refs to mailchimp in campaign submodule](https://www.drupal.org/node/2884660)
* [Remove undefined func in campaignmonitor_campaign](https://www.drupal.org/node/2883787)

## 8.x-2.0-alpha1

* Create Campaignmonitor connector as a service
* Remove sub modules
