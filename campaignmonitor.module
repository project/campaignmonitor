<?php

/**
 * @file
 * Module that plugs in Campaign Monitor functionality to your Drupal web site.
 *
 * For Campaign Monitor information see: http://www.campaignmonitor.com/.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_help().
 */
function campaignmonitor_help($route_name, RouteMatchInterface $route_match) {
  $output = '';

  switch ($route_name) {
    // Main module help for the facets module.
    case 'campaignmonitor.admin':
      $output = '<p>' . t('Use your API key and other keys to have users register for a mailing list setup through
      Campaign Monitor.') . '</p>';
      break;

    case 'help.page.campaignmonitor':
      $output = '<p>' . t('In order for a newsletter list to be available for selection on the site it needs to be
    enabled from <a href="@admin">Lists</a>', ['@admin' => '/admin/config/services/campaignmonitor/lists']
        ) .
        '</p>';
      break;
  }

  return $output;

}

/**
 * Implements hook_theme().
 */
function campaignmonitor_theme($existing, $type, $theme, $path) {
  return [
    'campaignmonitor_subscribe_form' => [
      'render element' => 'form',
    ],
  ];
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function campaignmonitor_user_delete(EntityInterface $entity) {
  // Unsubscribe users when deleted.
  $mail = $entity->get('mail')->getValue()[0]['value'];
  $campaignManager = \Drupal::service('campaignmonitor.manager');
  $campaignSubscriptionManager = \Drupal::service('campaignmonitor.subscription_manager');
  $lists = $campaignManager->getLists();
  foreach ($lists as $list_id => $values) {
    $campaignSubscriptionManager->userUnsubscribe($list_id, $mail);
  }
}
