<?php

namespace Drupal\campaignmonitor\Form;

use Drupal\campaignmonitor\CampaignMonitorManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure campaignmonitor settings for this site.
 */
class CampaignMonitorAdminForm extends ConfigFormBase {

  /**
   * The campaignmonitor manager.
   *
   * @var \Drupal\campaignmonitor\CampaignMonitorManager
   */
  protected $campaignMonitorManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\campaignmonitor\CampaignMonitorManager $campaignmonitor_manager
   *   The Campaign Monitor manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CampaignMonitorManager $campaignmonitor_manager) {
    $this->configFactory = $config_factory;
    $this->campaignMonitorManager = $campaignmonitor_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('campaignmonitor.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'campaignmonitor_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['campaignmonitor.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('campaignmonitor.settings');

    $cm_api_url = Url::fromUri('https://help.campaignmonitor.com/topic.aspx?t=206',
      ['attributes' => ['target' => '_blank']]);

    $form['campaignmonitor_account'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Account details'),
      '#description' => $this->t('Enter your Campaign Monitor account information. See @link for more information.', ['@link' => Link::fromTextAndUrl($this->t('the Campaign Monitor API documentation'), $cm_api_url)->toString()]),
      // '#collapsible' => empty($config) ? FALSE : TRUE,
      // '#collapsed' => empty($config) ? FALSE : TRUE,.
      '#tree' => TRUE,
    ];

    $form['campaignmonitor_account']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('Your Campaign Monitor API Key. See <a href="https://help.campaignmonitor.com/api-keys">documentation</a>.'),
      '#default_value' => $config->get('api_key'),
      '#required' => TRUE,
      '#size' => 250,
      '#maxlength' => 250,
    ];

    $form['campaignmonitor_account']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('Your Campaign Monitor Client ID. See <a href="https://help.campaignmonitor.com/api-keys">documentation</a>.'),
      '#default_value' => $config->get('client_id') != NULL ? $config->get('client_id') : '',
      '#required' => TRUE,
    ];

    if ($config->get('client_id') != NULL) {

      $form['campaignmonitor_general'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('General settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#tree' => TRUE,
      ];

      $form['campaignmonitor_general']['cache_timeout'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Cache timeout'),
        '#description' => $this->t('Cache timeout in seconds for stats, subscribers and archive information.'),
        '#size' => 4,
        '#default_value' => $config->get('cache_timeout') != NULL ? $config->get('cache_timeout') : '360',
      ];

      $form['campaignmonitor_general']['archive'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Newsletter archive'),
        '#description' => $this->t('Create a block with links to HTML versions of past campaigns.'),
        '#default_value' => $config->get('archive') != NULL ? $config->get('archive') : 0,
      ];

      $form['campaignmonitor_general']['logging'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Log errors'),
        '#description' => $this->t('Log communication errors with the Campaign Monitor service, if any.'),
        '#default_value' => $config->get('logging') != NULL ? $config->get('logging') : 0,
      ];

      $form['campaignmonitor_general']['instructions'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Newsletter instructions'),
        '#description' => $this->t('This message will be displayed to the user when subscribing to newsletters.'),
        '#default_value' => $config->get('instructions') != NULL ? $config->get('instructions') : $this->t('Select the
        newsletters you want to subscribe to.'),
      ];

      $form['campaignmonitor_general']['subscription_confirmation_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Subscription confirmation text'),
        '#description' => $this->t('Subscription confirmation text after user subscribed to newsletter. Use @name to specify list name, @interests to specify interests.'),
        '#default_value' => $config->get('subscription_confirmation_text') != NULL
          ? $config->get('subscription_confirmation_text')
          : 'You are subscribed to the @name list.',
      ];

      // Add cache clear button.
      $form['clear_cache'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Clear cached data'),
        '#description' => $this->t('The information downloaded from Campaign Monitor is cached to speed up the website. The lists details, custom fields and other data may become outdated if these are changed at Campaign Monitor. Clear the cache to refresh this information.'),
      ];

      $form['clear_cache']['clear'] = [
        '#type' => 'submit',
        '#value' => $this->t('Clear cached data'),
        '#submit' => ['::submitFormClearCache'],
      ];
    }

    $form['cron'] = [
      '#type' => 'checkbox',
      '#title' => 'Use batch processing.',
      '#description' => $this->t('Puts all campaignmonitor subscription operations into the cron queue. (Includes subscribe, update, and unsubscribe operations.) <i>Note: May cause confusion if caches are cleared, as requested changes will appear to have failed until cron is run.</i>'),
      '#default_value' => $config->get('cron'),
    ];
    $form['batch_limit'] = [
      '#type' => 'select',
      '#options' => [
        '1' => '1',
        '10' => '10',
        '25' => '25',
        '50' => '50',
        '75' => '75',
        '100' => '100',
        '250' => '250',
        '500' => '500',
        '750' => '750',
        '1000' => '1000',
        '2500' => '2500',
        '5000' => '5000',
        '7500' => '7500',
        '10000' => '10000',
      ],
      '#title' => $this->t('Batch limit'),
      '#description' => $this->t('Maximum number of entities to process in a single cron run. campaignmonitor suggest keeping this at 5000 or below. <i>This value is also used for batch Merge Variable updates on the Fields tab (part of campaignmonitor_lists).</i>'),
      '#default_value' => $config->get('batch_limit'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * The submit handler for the clearing the cache.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function submitFormClearCache(array &$form, FormStateInterface $form_state) {
    $this->campaignMonitorManager->clearCache();
  }

  /**
   * The submit handler for the admin form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('campaignmonitor.settings');
    $config
      ->set('api_key', $form_state->getValue([
        'campaignmonitor_account',
        'api_key',
      ]))
      ->set('client_id', $form_state->getValue([
        'campaignmonitor_account',
        'client_id',
      ]))
      ->set('cache_timeout', $form_state->getValue([
        'campaignmonitor_general',
        'cache_timeout',
      ]))
      ->set('library_path', $form_state->getValue([
        'campaignmonitor_general',
        'library_path',
      ]))
      ->set('archive', $form_state->getValue([
        'campaignmonitor_general',
        'archive',
      ]))
      ->set('logging', $form_state->getValue([
        'campaignmonitor_general',
        'logging',
      ]))
      ->set('instructions', $form_state->getValue([
        'campaignmonitor_general',
        'instructions',
      ]))
      ->set('subscription_confirmation_text', $form_state->getValue([
        'campaignmonitor_general',
        'subscription_confirmation_text',
      ]))
      ->set('cron', $form_state->getValue('cron'))
      ->set('batch_limit', $form_state->getValue('batch_limit'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
