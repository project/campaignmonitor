<?php

namespace Drupal\campaignmonitor\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\campaignmonitor\CampaignMonitorManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form for deleting mymodule data.
 */
class CampaignMonitorListDeleteForm extends ConfirmFormBase {

  /**
   * The campaign monitor manager.
   *
   * @var Drupal\campaignmonitor\CampaignMonitorManager
   */
  protected $campaignMonitorManager;

  /**
   * The ID of the item to delete.
   *
   * @var string
   */
  protected $id;

  /**
   * Class constructor.
   */
  public function __construct(
    CampaignMonitorManager $campaignmonitor_manager
  ) {

    $this->campaignMonitorManager = $campaignmonitor_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('campaignmonitor.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'campaignmonitor_list_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $options = $this->campaignMonitorManager->getExtendedList($this->id);
    return $this->t('Do you want to delete %name?', ['%name' => $options['name']]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('campaignmonitor.lists');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Only do this if you are sure!');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of form.
   * @param int $list_id
   *   (optional) The ID of the item to be deleted.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $list_id = NULL) {
    $this->id = $list_id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->campaignMonitorManager->deleteList($this->id)) {
      $this->messenger()->addStatus($this->t('The list has been deleted.'));
    }
    else {
      $this->messenger()->addError($this->t('The list could not be deleted.'));
    }
    $url = Url::fromRoute('campaignmonitor.refresh_lists');
    $form_state->setRedirectUrl($url);
  }

}
