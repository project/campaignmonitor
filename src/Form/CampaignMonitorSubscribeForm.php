<?php

namespace Drupal\campaignmonitor\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Utility\Token;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\campaignmonitor\CampaignMonitorManager;
use Drupal\campaignmonitor\CampaignMonitorSubscriptionManager;
use Drupal\Core\Render\RendererInterface;

/**
 * Subscribe to a campaignmonitor list.
 */
class CampaignMonitorSubscribeForm extends FormBase {

  /**
   * The form id which set as class property so it can be overwritten as needed.
   *
   * @var string
   */
  private $formId = 'campaignmonitor_subscribe_form';

  /**
   * The current user.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The campaign monitor manager service.
   *
   * @var Drupal\campaignmonitor\CampaignMonitorManager
   */
  protected $campaignMonitorManager;

  /**
   * The campaign monitor subscription manager service.
   *
   * @var Drupal\campaignmonitor\CampaignMonitorSubscriptionManager
   */
  protected $campaignMonitorSubscriptionManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module manager service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Class constructor.
   */
  public function __construct(CampaignMonitorManager $campaignmonitor_manager,
    CampaignMonitorSubscriptionManager $campaignmonitor_subscription_manager,
    AccountProxyInterface $account,
    EntityTypeManagerInterface $entity_type_manager,
    Token $token,
    ModuleHandlerInterface $module_handler,
    RendererInterface $renderer) {

    $this->campaignMonitorManager = $campaignmonitor_manager;
    $this->campaignMonitorSubscriptionManager = $campaignmonitor_subscription_manager;
    $this->account = $account;
    $this->entityTypeManager = $entity_type_manager;
    $this->token = $token;
    $this->moduleHandler = $module_handler;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('campaignmonitor.manager'),
      $container->get('campaignmonitor.subscription_manager'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('module_handler'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->formId;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormId($formId) {
    $this->formId = $formId;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['campaignmonitor.subscribe'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $config = []) {

    $email = '';
    // Try to get the email address from the user object.
    if ($this->account->id() != 0) {
      $account = $this->entityTypeManager->getStorage('user')->load($this->account->id());
      $email = $account->get('mail')->getValue()[0]['value'];
    }

    // Use of key 'mail' here corresponds with key used in registration form.
    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#maxlength' => 200,
      '#default_value' => $email,
    ];

    switch ($config['list']) {
      case 'single':
        $form += $this->singleSubscribeForm($config, $email);
        $this->setFormID($this->formId . '_single');
        break;

      default:
        $form += $this->userSelectSubscribeForm($form, $form_state, $config, $email);
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Subscribe'),
    ];

    $form['config'] = [
      '#type' => 'hidden',
      '#value' => serialize($config),
    ];

    $this->renderer->addCacheableDependency($form, $config);

    return $form;
  }

  /**
   * Returns form with single subscribe option.
   */
  protected function singleSubscribeForm($config, $email) {
    return $this->campaignMonitorSubscriptionManager->singleSubscribeForm($config, $email);
  }

  /**
   * Return form as per user select subscribe form.
   */
  protected function userSelectSubscribeForm(array $form, FormStateInterface $form_state, $config, $email) {
    $form = [];

    // Set options for the form.
    $form = [
      '#tree' => TRUE,
      '#attributes' => [
        'class' => [
          'campaignmonitor-subscribe-form',
          'campaignmonitor-subscribe-form-all-lists',
        ],
      ],
    ];

    $lists = $this->campaignMonitorManager->getLists();

    $options = [];
    foreach ($lists as $list_id => $list) {
      if ($this->campaignMonitorManager->isListEnabled($list_id)) {
        $options[$list_id] = $list['name'];
      }
    }

    $subscriptions = $this->campaignMonitorManager->getUserSubscriptions($email);
    $defaults = [];
    foreach ($subscriptions as $key => $value) {
      if (!empty($value)) {
        $defaults[] = $value;
      }
    }

    $form['selection'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => $this->t('Lists'),
      '#default_value' => $defaults,
    // '#required' => TRUE,.
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->campaignMonitorSubscriptionManager->subscribeSubmitHandler($form, $form_state);
  }

}
