<?php

namespace Drupal\campaignmonitor\Form;

use Drupal\campaignmonitor\CampaignMonitorManager;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure campaignmonitor settings for this site.
 */
class CampaignMonitorListSettingsForm extends ConfigFormBase {

  /**
   * The campaign monitor manager.
   *
   * @var Drupal\campaignmonitor\CampaignMonitorManager
   */
  protected $campaignMonitorManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The module manager service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Class constructor.
   */
  public function __construct(
    CampaignMonitorManager $campaignmonitor_manager,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    ConfigFactoryInterface $config_factory
  ) {

    $this->campaignMonitorManager = $campaignmonitor_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('campaignmonitor.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'campaignmonitor_list_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['campaignmonitor.settings.list'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $list_id = NULL) {

    $form = ['#tree' => TRUE];

    // Reset lists so that we can make changes with this form.
    // This will ensure that settings come direct from CM and not cache.
    $this->campaignMonitorManager->getLists([], TRUE);
    $defaults = $this->campaignMonitorManager->getListSettings($list_id);
    $list = $this->campaignMonitorManager->getExtendedList($list_id);

    // Add list id to the form.
    $form['listId'] = [
      '#type' => 'hidden',
      '#value' => $list_id,
    ];

    // Set this form name (index).
    $form_key = 'campaignmonitor_list_' . $list_id;

    $form[$form_key]['status'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Enable list'),
      '#description' => $this->t('Enable the list to configure it and use it on the site.'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form[$form_key]['status']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#default_value' => isset($defaults['status']['enabled']) ? $defaults['status']['enabled'] : 0,
      '#attributes' => ['class' => ['enabled-list-checkbox']],
    ];

    $form[$form_key]['options'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('List options'),
      '#description' => $this->t('Changing the values will result in an update of the values on the Campaign Monitor homepage.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#states' => [
        'visible' => [
          '.enabled-list-checkbox' => ['checked' => TRUE],
        ],
      ],
    ];

    $form[$form_key]['options']['listname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List name'),
      '#default_value' => $list['name'],
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="status[enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form[$form_key]['options']['UnsubscribePage'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unsubscribe page'),
      '#default_value' => $list['details']['UnsubscribePage'],
    ];

    $form[$form_key]['options']['ConfirmationSuccessPage'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Confirmation success page'),
      '#default_value' => $list['details']['ConfirmationSuccessPage'],
    ];

    $form[$form_key]['options']['ConfirmedOptIn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Confirmed Opt In'),
      '#description' => $this->t('Selecting this will mean that subscribers will need to confirm their email each time they
      subscribe to the list'),
      '#default_value' => $list['details']['ConfirmedOptIn'],
    ];

    $form[$form_key]['display'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Display options'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#states' => [
        'visible' => [
          '.enabled-list-checkbox' => ['checked' => TRUE],
        ],
      ],
    ];

    $form[$form_key]['display']['name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Name field'),
      '#description' => $this->t('Whether the Name field should be displayed when subscribing.'),
      '#default_value' => isset($defaults['display']['name']) ? $defaults['display']['name'] : 0,
      '#attributes' => ['class' => ['tokenable', 'tokenable-name']],
    ];

    $form[$form_key]['display']['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('A description to accompany the list in forms.'),
      '#default_value' => isset($defaults['display']['description']) ? $defaults['display']['description'] : '',
      '#attributes' => ['class' => ['tokenable', 'tokenable-description']],
    ];

    $field_map = $this->entityFieldManager->getFieldMap();
    $user_field_map = $field_map['user'];
    $user_fields = array_keys($user_field_map);
    array_unshift($user_fields, 'None');
    foreach ($user_fields as $key => $user_field) {
      unset($user_fields[$key]);
      $user_fields[$user_field] = $user_field;
    }
    if (!empty($list['CustomFields'])) {
      $options = [];
      foreach ($list['CustomFields'] as $key => $field) {
        // Form API can't handle keys with [] in all cases.
        $token_form_key = str_replace(['[', ']'], '', $key);
        $user_fields[$token_form_key] = 'Custom Field: ' . $field['FieldName'];
      }
    }
    $form[$form_key]['display']['fname_field'] = [
      '#type' => 'select',
      '#options' => $user_fields,
      '#title' => $this->t('First name field'),
      '#description' => $this->t('The name that will be used by Campaign Monitor as a salutation in emails sent out. If using custom fields, they will not appear unless selected in custom field config below.'),
      '#default_value' => isset($defaults['display']['fname_field']) ? $defaults['display']['fname_field'] : 0,
    ];
    $form[$form_key]['display']['lname_field'] = [
      '#type' => 'select',
      '#options' => $user_fields,
      '#title' => $this->t('Last name field'),
      '#description' => $this->t('The name that will be used by Campaign Monitor as a salutation in emails sent out. If using custom fields, they will not appear unless selected in custom field config below or none to leave empty.'),
      '#default_value' => isset($defaults['display']['lname_field']) ? $defaults['display']['lname_field'] : 0,
    ];

    // List custom fields.
    if (!empty($list['CustomFields'])) {

      $options = [];
      foreach ($list['CustomFields'] as $key => $field) {
        // Form API can't handle keys with [] in all cases.
        $token_form_key = str_replace(['[', ']'], '', $key);
        $options[$token_form_key] = $field['FieldName'];
      }

      $form[$form_key]['CustomFields'] = [
        '#type' => 'table',
        '#caption' => $this->t('Custom fields - drag to arrange order'),
        '#header' => [
          $this->t('Include'),
          $this->t('Require'),
          $this->t('Weight'),
        ],
        '#empty' => $this->t('No items.'),
        '#tableselect' => FALSE,
        '#tabledrag' => [
          [
            'action' => 'order',
            'relationship' => 'sibling',
            'group' => 'group-order-weight',
          ],
        ],
        '#states' => [
          'visible' => [
            '.enabled-list-checkbox' => ['checked' => TRUE],
          ],
        ],
      ];
      // Use defaults if exists to get the order correct.
      if (isset($defaults['CustomFields'])) {
        $new_options = [];
        foreach ($defaults['CustomFields'] as $key => $values) {
          $new_options[$key] = $options[$key];
        }
        $options = $new_options;
      }
      foreach ($options as $key => $option) {
        $form[$form_key]['CustomFields'][$key]['#attributes']['class'][] = 'draggable';
        $form[$form_key]['CustomFields'][$key]['#weight'] = $defaults['CustomFields'][$key]['weight'] ?? 0;
        $form[$form_key]['CustomFields'][$key]['selected'] = [
          '#type' => 'checkbox',
          '#title' => $option,
          '#default_value' => isset($defaults['CustomFields'][$key]['selected']) ? $defaults['CustomFields'][$key]['selected'] : 0,
          '#attributes' => ['class' => ['tokenable', 'tokenable-custom-fields']],
        ];
        $form[$form_key]['CustomFields'][$key]['required'] = [
          '#type' => 'checkbox',
          '#title' => $option,
          '#default_value' => isset($defaults['CustomFields'][$key]['required']) ? $defaults['CustomFields'][$key]['required'] : 0,
        ];
        $form[$form_key]['CustomFields'][$key]['weight'] = [
          '#type' => 'weight',
          '#title' => $this->t('Weight for @title', ['@title' => $option]),
          '#title_display' => 'invisible',
          '#default_value' => isset($defaults['CustomFields'][$key]['weight']) ? $defaults['CustomFields'][$key]['weight'] : 0,
          '#attributes' => ['class' => ['group-order-weight']],
        ];
      }

    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * Edit form validation handler which calls the API to save the information
   * that was entered. This is done in the validation function so we can give
   * better feedback to the user and to prevent the user from having to enter
   * the information once more on failure.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $list_id = $form_state->getValue('listId');
    $values = $form_state->getValues();
    // Build array with basic information.
    $values = $values['campaignmonitor_list_' . $list_id];
    $options = [
      'Title' => HTML::escape($values['options']['listname']),
      'UnsubscribePage' => HTML::escape($values['options']['UnsubscribePage']),
      'ConfirmedOptIn' => $values['options']['ConfirmedOptIn'] ? TRUE : FALSE,
      'ConfirmationSuccessPage' => HTML::escape($values['options']['ConfirmationSuccessPage']),
    ];
    $result = $this->campaignMonitorManager->updateList($list_id, $options);

    if ($result != 'success') {
      $form_state->setErrorByName('', $result);
    }

    // Redirect to list overview.
    $url = Url::fromRoute('campaignmonitor.lists');
    $form_state->setRedirectUrl($url);
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $list_id = $form_state->getValue('listId');

    $values = $form_state->getValues();
    $values = $values['campaignmonitor_list_' . $list_id];
    // These are saved remotely.
    unset($values['options']);

    $list_config = $this->configFactory->getEditable('campaignmonitor.settings.list');
    $list_key = $this->campaignMonitorManager->listKey($list_id);
    $list_config->set($list_key, $values)->save();

    parent::submitForm($form, $form_state);
  }

}
