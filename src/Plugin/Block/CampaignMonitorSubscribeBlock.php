<?php

namespace Drupal\campaignmonitor\Plugin\Block;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\campaignmonitor\CampaignMonitorManager;
use Drupal\campaignmonitor\CampaignMonitorSubscriptionManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'Subscribe' block.
 *
 * @Block(
 *   id = "campaignmonitor_subscribe_block",
 *   admin_label = @Translation("Subscribe Block"),
 *   category = @Translation("Campaign Monitor Signup"),
 *   module = "campaignmonitor",
 * )
 */
class CampaignMonitorSubscribeBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The campaignmonitor manager service.
   *
   * @var Drupal\campaignmonitor\CampaignMonitorManager
   */
  protected $campaignMonitorManager;

  /**
   * The campaign monitor subscribe manager.
   *
   * @var Drupal\campaignmonitor\CampaignMonitorSubscriptionManager
   */
  protected $campaignMonitorSubscriptionManager;

  /**
   * Vendor/campaignmonitor/create-send-php.
   *
   * @var mixed
   */
  protected $createSendManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a CampaignMonitorSubscribeBlock object.
   *
   * @param mixed[] $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\campaignmonitor\CampaignMonitorManager $campaignmonitor_manager
   *   The campaign monitor manager.
   * @param \Drupal\campaignmonitor\CampaignMonitorSubscriptionManager $campaignmonitor_subscription_manager
   *   The campaign monitor subscription manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CampaignMonitorManager $campaignmonitor_manager, CampaignMonitorSubscriptionManager $campaignmonitor_subscription_manager, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->campaignMonitorManager = $campaignmonitor_manager;
    $this->createSendManager = $this->campaignMonitorManager->createClientObj();
    $this->campaignMonitorSubscriptionManager = $campaignmonitor_subscription_manager;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Instantiates this form class.
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('campaignmonitor.manager'),
      $container->get('campaignmonitor.subscription_manager'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form += $this->campaignMonitorSubscriptionManager->subscribeSettingsForm($config);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    $subscribe_option = $form_state->getValue(['campaignmonitor', 'list']);
    if ($subscribe_option == 'single') {
      $list_id = $form_state->getValue(['campaignmonitor', 'list_id']);
      if (empty($list_id)) {
        $form_state->setErrorByName('settings[campaignmonitor][list_id]', 'List Selection required');
      }
    }
    if ($subscribe_option == 'segments') {
      $segment_list_id = $form_state->getValue(['campaignmonitor', 'segment_list_id']);
      if (empty($segment_list_id)) {
        $form_state->setErrorByName('settings[campaignmonitor][segment_list_id]', 'List Selection required');
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['list'] = $form_state->getValue([
      'lists',
      'list',
    ]);
    $this->configuration['list_id'] = $form_state->getValue([
      'lists',
      'list_id',
    ]);
    $this->configuration['list_id_text'] = $form_state->getValue([
      'lists',
      'list_id_text',
    ]);
    $this->configuration['segment_list_id'] = $form_state->getValue([
      'lists',
      'segment_list_id',
    ]);
    $this->configuration['custom_fields_visible'] = $form_state->getValue([
      'custom_fields',
      'custom_fields_visible',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();
    $content = $this->formBuilder
      ->getForm('\Drupal\campaignmonitor\Form\CampaignMonitorSubscribeForm', $config);

    return $content;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['user']);
  }

}
