<?php

namespace Drupal\campaignmonitor;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Manager for Campaignmonitor.
 */
class CampaignMonitorManager {

  use StringTranslationTrait;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The module manager service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Config settings.
   *
   * @var mixed
   */
  protected $config;

  /**
   * Stores the api key.
   *
   * @var mixed
   */
  protected $apiKey;

  /**
   * Stores the client id.
   *
   * @var mixed
   */
  protected $clientId;

  /**
   * Tells if there is any log errors.
   *
   * @var bool
   */
  protected $logErrors = FALSE;

  /**
   * This variable stores the errors.
   *
   * @var array
   */
  protected $errors = [];

  /**
   * These variables are used as static cache for the object.
   *
   * @var array
   */
  protected $lists = [];

  /**
   * List stats.
   *
   * @var array
   */
  protected $listStats = [];

  /**
   * This variable is used to store campaign data in cache.
   *
   * @var array
   */
  protected $campaigns = [];

  /**
   * This variable is used to store draft data.
   *
   * @var array
   */
  protected $drafts = [];

  /**
   * This variables used to store the subscriber details.
   *
   * @var array
   */
  protected $subscribers = [];

  /**
   * Holds the object instance (part of the singleton pattern).
   *
   * @var object
   */
  protected static $instance;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The loggers service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, CacheBackendInterface $cache_backend, LanguageManagerInterface $language_manager, ModuleHandlerInterface $module_handler) {

    $this->loggerFactory = $logger_factory;
    $this->cacheBackend = $cache_backend;
    $this->languageManager = $language_manager;
    $this->moduleHandler = $module_handler;

    $this->configFactory = $config_factory;
    // Get account information.
    $this->config = $config_factory->get('campaignmonitor.settings');

    // Get API key/client ID if they are defined.
    $this->apiKey = $this->config->get('api_key');
    $this->clientId = $this->config->get('client_id');

    // Enable logging.
    if ($this->config->get('logging')) {
      $this->logErrors = $this->config->get('logging');
    }

  }

  /**
   * Add an error to the local stack and call watchdog, if logging is enabled.
   *
   * @param string $message
   *   The error message.
   * @param int $code
   *   Normally the HTTP response code.
   */
  protected function addError($message, $code = -1) {
    $this->errors[] = [
      'code' => $code,
      'message' => $message,
    ];

    if ($this->logErrors) {
      $msg = $this->t('Failed with code: @code and message: @msg', [
        '@code' => $code,
        '@msg' => $message,
      ]);
      $this->loggerFactory->get('campaignmonitor')
        ->notice($msg);
    }
  }

  /**
   * Create API client object.
   *
   * @return \CS_REST_Clients
   *   The Campaign Monitor client object.
   */
  public function createClientObj() {
    return ($this->clientId) ? new \CS_REST_Clients($this->clientId, $this->apiKey) : NULL;
  }

  /**
   * Create API list object.
   *
   * @param string $listId
   *   The Campaign Monitor list ID.
   *
   * @return \CS_REST_Lists
   *   The Campaign Monitor list object.
   */
  protected function createListObj($listId) {
    return new \CS_REST_Lists($listId, $this->apiKey);
  }

  /**
   * Create API subscribers object.
   *
   * @param string $listId
   *   The Campaign Monitor list ID.
   *
   * @return \CS_REST_Subscribers
   *   The Campaign Monitor subscriber object.
   */
  protected function createSubscriberObj($listId) {
    return new \CS_REST_Subscribers($listId, $this->apiKey);
  }

  /**
   * Create a UNIX timestamp based on the cache timeout from admin interface.
   *
   * @return string
   *   A UNIX timestamp.
   */
  protected function getCacheTimeout() {
    return time() + ($this->config->get('cache_timeout') ? (int) $this->config->get('cache_timeout') : 360);
  }

  /**
   * Gets the latest error from the stack of possible errors.
   *
   * Errors that were encountered during communication with CM servers.
   *
   * @return array
   *   An array containing an error code and message.
   */
  public function getLatestError() {
    if (!empty($this->errors)) {
      $last = $this->errors[count($this->errors) - 1];
      return [
        'code' => $last['code'],
        'message' => $last['message'],
      ];
    }
    else {
      return [
        'code' => 1,
        'message' => $this->t('There do not seem to be any errors.'),
      ];
    }
  }

  /**
   * Returns the internal error array with the format below.
   *
   * $errors[] = array(
   *   'type' => [watchdog error type],
   *   'code' => [error code],
   *   'message' => [message],
   * );
   *
   * @return mixed|array|false
   *   An array of errors or FALSE if the array is empty.
   */
  public function getErrors() {
    if (count($this->errors)) {
      return $this->errors;
    }
    return FALSE;
  }

  /**
   * Resets the internal error array to an empty array.
   */
  public function resetErrors() {
    $this->errors = [];
  }

  /**
   * Gets all CM lists found under the client ID given during object creation.
   *
   * The list is returned as a keyed array and cached in the
   * cache table, so it may not always return the newest information.
   *
   * The array has the format below. Be aware that the same cache is used for
   * the getListDetails() and getCustomFields() functions. This means that the
   * information returned by this function may contain an extended list of
   * information if any of these functions have been called.
   *
   * $list[$id] = array(
   *  'name' => 'List name',
   * );
   *
   * @return array|false
   *   An array of lists available from Campaign Monitor or FALSE on failure.
   */
  public function getLists($list_ids = [], $reset = FALSE) {
    if (empty($this->lists)) {

      if (!$reset && $cache = $this->cacheBackend->get('campaignmonitor_lists')) {
        // Cache information found.
        $this->lists = $cache->data;
      }
      else {
        // Create list object and get the lists, then save the lists in the
        // local cache.
        if ($obj = $this->createClientObj()) {

          $result = $obj->get_lists();
          if ($result->was_successful()) {
            foreach ($result->response as $list) {
              $this->lists[$list->ListID] = [
                'name' => $list->Name,
              ];
            }
            $this->cacheBackend->set('campaignmonitor_lists', $this->lists);
          }
          else {
            $this->addError($result->response->Message, $result->http_status_code);
          }
        }
        else {
          return [];
        }
      }
    }
    // Filter by given IDs.
    if (!empty($list_ids)) {
      $filtered_lists = [];
      foreach ($list_ids as $id) {
        if (array_key_exists($id, $list_ids)) {
          $filtered_lists[$id] = $list_ids[$id];
        }
      }
      return $filtered_lists;
    }

    return $this->lists;
  }

  /**
   * Gets list details from Campaign Monitor.
   *
   * This information is retrieved from the local cache and may be outdated.
   * It fetches the unsubscribe link, confirmation success page and
   * confirmed opt-in options.
   *
   * @param string $listId
   *   The Campaign Monitor list ID.
   *
   * @return mixed|array|false
   *   An array with the information or FALSE on failure.
   */
  public function getListDetails($listId) {
    // If lists have not been loaded yet, get them as they build the basic
    // cache.
    if (empty($this->lists)) {
      $this->getLists();
    }

    // Test that the listId is valid.
    if (!isset($this->lists[$listId])) {
      $this->addError($this->t('Unknown list id @listID.', ['@listID' => $listId]));
      return FALSE;
    }

    // If list details are not set, create list object and fetch the information
    // from the Campaign Monitor servers.
    if (!isset($this->lists[$listId]['details'])) {
      if ($obj = $this->createListObj($listId)) {
        $result = $obj->get();
        if ($result->was_successful()) {
          // Convert the return object into a keyed array.
          $this->lists[$listId]['details'] = [];
          foreach ($result->response as $key => $value) {
            if (!in_array($key, ['ListID', 'Title'])) {
              $this->lists[$listId]['details'][$key] = $value;
            }
          }

          // Update the cache with list details.
          $this->cacheBackend->set('campaignmonitor_lists', $this->lists);
        }
        else {
          $this->addError($result->response->Message, $result->http_status_code);
          return FALSE;
        }
      }
      else {
        return FALSE;
      }
    }

    return $this->lists[$listId]['details'];
  }

  /**
   * Gets the settings for a list.
   */
  public function getListSettings($list_id, $config = NULL) {
    $list_key = $this->listKey($list_id);
    if (!$config) {
      // Get list config.
      $config = $this->configFactory->get('campaignmonitor.settings.list');
    }
    return $config->get($list_key);
  }

  /**
   * Sets the config for a list.
   *
   * @param string $list_id
   *   The Campaign Monitor list ID.
   * @param mixed $list_options
   *   The options in list.
   */
  public function setListSettings($list_id, $list_options) {
    $config = $this->configFactory
      ->getEditable('campaignmonitor.settings.list');
    $list_key = $this->listKey($list_id);
    $config->set($list_key, $list_options)
      ->save();
  }

  /**
   * Helper function to determine config var name for list options.
   *
   * @param string $list_id
   *   The Campaign Monitor list ID.
   *
   * @return string
   *   Config variable name.
   */
  public function listKey($list_id) {
    return 'campaignmonitor_list_' . $list_id;
  }

  /**
   * Fetch custom fields for a given list.
   *
   * Store this information locally in the list cache.
   * The information is stored as a keyed array on the list
   * array under the "CustomFields" key.
   *
   * @param string $listId
   *   The Campaign Monitor list ID.
   *
   * @return mixed|array|false
   *   An array with the information or FALSE on failure.
   */
  public function getCustomFields($listId) {
    // If the lists have not been loaded yet, get them as they build the basic
    // cache.
    if (empty($this->lists)) {
      $this->getLists();
    }

    // Test that the listId is valid.
    if (!isset($this->lists[$listId])) {
      $this->addError($this->t('Unknown list id @listID.', ['@listID' => $listId]));
      return FALSE;
    }

    // If custom fields are not set on the list, then create the list object and
    // fetch custom fields into a keyed array.
    if (!isset($this->lists[$listId]['CustomFields'])) {
      if ($obj = $this->createListObj($listId)) {
        $result = $obj->get_custom_fields();
        if ($result->was_successful()) {
          $this->lists[$listId]['CustomFields'] = [];
          foreach ($result->response as $field) {
            foreach ($field as $name => $details) {
              $this->lists[$listId]['CustomFields'][$field->Key][$name] = $details;
            }
          }

          // Update cache with list details.
          $this->cacheBackend->set('campaignmonitor_lists', $this->lists);
        }
        else {
          $this->addError($result->response->Message, $result->http_status_code);
        }
      }
      else {
        return FALSE;
      }
    }

    return $this->lists[$listId]['CustomFields'];
  }

  /**
   * Get all information available about a given list.
   *
   * This is done by calling getLists(), getListDetails()
   * and getCustomFields(), hence building the local list cache.
   *
   * @param string $listId
   *   The Campaign Monitor list ID.
   *
   * @return mixed|array|false
   *   An array containing the list information or FALSE on failure.
   */
  public function getExtendedList($listId) {
    // If the lists have not been loaded yet, get them as they build the basic
    // cache.
    if (empty($this->lists)) {
      $this->getLists();
    }

    // Test that the listId is valid.
    if (!isset($this->lists[$listId])) {
      $this->addError($this->t('Unknown list id @listID.', ['@listID' => $listId]));
      return FALSE;
    }

    // Load list details and custom fields (using is_array() since
    // getCustomFields() may return an empty array).
    if (!$this->getListDetails($listId) || !is_array($this->getCustomFields($listId))) {
      $this->addError($this->t('Could not retrieve extended information for @listID.', ['@listID' => $listId]));
      return FALSE;
    }

    return $this->lists[$listId];
  }

  /**
   * Update remote list information.
   *
   * The options array should have the fields "Title", "UnsubscribePage",
   * "ConfirmedOptIn" and "ConfirmationSuccessPage".
   * If you do not wish to set these use an empty string.
   *
   * @param string $listId
   *   The Campaign Monitor list ID.
   * @param array $options
   *   An array of options with information to update.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise.
   */
  public function updateList($listId, array $options = []) {
    // Make sure that list is loaded.
    if (!$this->getListDetails($listId)) {
      $this->addError($this->t('Could not retrieve update list information for @listID.', ['@listID' => $listId]));
      return FALSE;
    }

    // Get list object and update the list.
    if ($obj = $this->createListObj($listId)) {
      // @todo: check that the options are correct.
      $result = $obj->update($options);
      if ($result->was_successful()) {

        // Update local list cache.
        $this->lists[$listId]['name'] = $options['Title'];
        $this->lists[$listId]['details']['UnsubscribePage'] = $options['UnsubscribePage'];
        $this->lists[$listId]['details']['ConfirmedOptIn'] = $options['ConfirmedOptIn'];
        $this->lists[$listId]['details']['ConfirmationSuccessPage'] = $options['ConfirmationSuccessPage'];

        // Update the cache.
        $this->cacheBackend->set('campaignmonitor_lists', $this->lists);
        return TRUE;
      }
      else {
        $this->addError($result->response->Message, $result->http_status_code);
      }
    }
    return FALSE;
  }

  /**
   * Fetch stats about a given list including subscribers and unsubscribers.
   *
   * This information is temporarily stored in the local cache.
   * The default timeout is 360 seconds.
   *
   * @param string $listId
   *   The Campaign Monitor list ID.
   *
   * @return mixed|array|false
   *   An array containing the stats or FALSE on failure.
   */
  public function getListStats($listId) {
    $fetch = FALSE;
    if (!isset($this->listStats[$listId])) {
      // Not found inside object, try the cache.
      if (($cache = $this->cacheBackend->get('campaignmonitor_list_stats')) && !empty($cache->data)) {
        // Cache information found.
        $this->listStats = $cache->data;
        if (!isset($this->listStats[$listId])) {
          // Not found inside cache either.
          $fetch = TRUE;
        }
      }
      else {
        // No cache found or expired.
        $fetch = TRUE;
      }
    }

    if ($fetch) {
      if ($obj = $this->createListObj($listId)) {
        // Get stats from Campaign Monitor.
        $result = $obj->get_stats();
        if ($result->was_successful()) {
          $this->listStats[$listId] = (array) $result->response;

          // Update the cache.
          $this->cacheBackend->set('campaignmonitor_list_stats', $this->listStats, $this->getCacheTimeout());
        }
        else {
          $this->addError($result->response->Message, $result->http_status_code);
          return FALSE;
        }
      }
      else {
        return FALSE;
      }
    }

    return $this->listStats[$listId];
  }

  /**
   * Checks if the list given by id is enabled locally.
   *
   * @param string $list_id
   *   The Campaign Monitor list ID.
   *
   * @return int
   *   1 if list is enabled, 0 otherwise.
   */
  public function isListEnabled($list_id) {
    $list_options = $this->getListSettings($list_id);
    return isset($list_options['status']['enabled']) ? $list_options['status']['enabled'] : 0;
  }

  /**
   * Delete a list from Campaign Monitor.
   *
   * This action can not be reverted. The
   * list is also removed from the local cache.
   *
   * @param mixed $listId
   *   The Campaign Monitor list ID.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise.
   */
  public function deleteList($listId) {
    if ($obj = $this->createListObj($listId)) {
      $result = $obj->delete();
      if ($result->was_successful()) {
        unset($this->lists[$listId]);
        $this->cacheBackend->set('campaignmonitor_lists', $this->lists, $this->getCacheTimeout());
        return TRUE;
      }
      else {
        $this->addError($result->response->Message, $result->http_status_code);
        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   * Create a new list at the Campaign Monitor servers.
   *
   * The side-effect: the local cache is cleared.
   *
   * @param string $title
   *   The title of the new list.
   * @param string $unsubscribePage
   *   An optional page to redirect subscribers to when they unsubscribe.
   * @param bool $confirmedOptIn
   *   Whether or not this list requires to confirm the subscription. Defaults
   *   to FALSE.
   * @param string $confirmationSuccessPage
   *   An optional page to redirect subscribers to when they confirm their
   *   subscription.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise.
   */
  public function createList($title, $unsubscribePage = '', $confirmedOptIn = FALSE, $confirmationSuccessPage = '') {
    if ($obj = $this->createListObj(NULL)) {
      $result = $obj->create($this->clientId, [
        'Title' => Html::escape($title),
        'UnsubscribePage' => Html::escape($unsubscribePage),
        'ConfirmedOptIn' => $confirmedOptIn,
        'ConfirmationSuccessPage' => Html::escape($confirmationSuccessPage),
      ]);
      if ($result->was_successful()) {
        // Clear the cache, so the list information can be retrieved again.
        $this->clearCache();
        return TRUE;
      }
      else {
        $this->addError($result->response->Message, $result->http_status_code);
        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   * Get basic information about campaigns in the form of a keyed array.
   *
   * The information is stored locally in a temporary cache.
   * The array is formatted like this:.
   *
   * $campaigns[$id] => array(
   *   'Name' => 'Campaign Name',
   *   'Subject' => 'Campaign subject line',
   *   'Sent' => 'Unix timestamp',
   *   'Recipients' => 'The number of recipients',
   *   'Link' => 'Online URL to the campaign',
   * );
   *
   * @return mixed|array|false
   *   An array with the campaigns or FALSE on failure.
   */
  public function getCampaigns() {
    if (empty($this->campaigns)) {
      if (($cache = $this->cacheBackend->get('campaignmonitor_campaigns')) && !empty($cache->data)) {
        // Cache information found.
        $this->campaigns = $cache->data;
      }
      else {
        if ($obj = $this->createClientObj()) {

          $result = $obj->get_campaigns();
          if ($result->was_successful()) {
            // Build an array for each campaign returned.
            foreach ($result->response as $campaign) {
              $this->campaigns[$campaign->CampaignID] = [
                'Name' => $campaign->Name,
                'Subject' => $campaign->Subject,
                'Sent' => strtotime($campaign->SentDate),
                'Recipients' => $campaign->TotalRecipients,
                'Link' => $campaign->WebVersionURL,
              ];
            }
            // Save campaigns in the cache.
            $this->cacheBackend->set('campaignmonitor_campaigns', $this->campaigns, $this->getCacheTimeout());
          }
          else {
            $this->addError($result->response->Message, $result->http_status_code);
            return FALSE;
          }
        }
        else {
          return FALSE;
        }
      }
    }
    return $this->campaigns;
  }

  /**
   * Get basic information about drafts in the form of a keyed array.
   *
   * The information is stored locally in a temporary cache. The array is
   * formatted like this:.
   *
   * $drafts[$id] => array(
   *   'Name' => 'Campaign Name',
   *   'Subject' => 'Campaign subject line',
   *   'Sent' => 'Unix timestamp',
   *   'Recipients' => 'The number of recipients',
   *   'Link' => 'Online URL to the campaign',
   * );
   *
   * @return mixed|array|false
   *   An array with the drafts or FALSE on failure.
   */
  public function getDrafts() {
    if (empty($this->drafts)) {
      if (($cache = $this->cacheBackend->get('campaignmonitor_drafts')) && !empty($cache->data)) {
        // Cache information found.
        $this->drafts = $cache->data;
      }
      else {
        if ($obj = $this->createClientObj()) {

          $result = $obj->get_drafts();
          if ($result->was_successful()) {
            // Build an array for each campaign returned.
            foreach ($result->response as $draft) {

              $this->drafts[$draft->CampaignID] = [
                'Name' => $draft->Name,
                'Subject' => $draft->Subject,
                'Created' => strtotime($draft->DateCreated),
                'From' => $draft->FromName,
                'Link' => $draft->PreviewURL,
              ];
            }
            // Save drafts in the cache.
            $this->cacheBackend->set('campaignmonitor_drafts', $this->drafts, $this->getCacheTimeout());
          }
          else {
            $this->addError($result->response->Message, $result->http_status_code);
            return FALSE;
          }
        }
        else {
          return FALSE;
        }
      }
    }
    return $this->drafts;
  }

  /**
   * Get values entered by the subscriber, when subscribing to a list.
   *
   * @param string $listId
   *   The unique Campaign Monitor list ID.
   * @param string $email
   *   The e-mail address that identifies the subscriber.
   *
   * @return mixed|array|false
   *   An array containing subscriber information or FALSE on failure.
   */
  public function getSubscriber($listId, $email) {
    $fetch = FALSE;
    if (!isset($this->subscribers[$listId . $email])) {
      // Not found inside object, try the cache.
      if (($cache = $this->cacheBackend->get('campaignmonitor_subscribers')) && !empty($cache->data)) {
        // Cache information found.
        $this->subscribers = $cache->data;
        if (!isset($this->subscribers[$listId . $email])) {
          // Not found inside cache either.
          $fetch = TRUE;
        }
      }
      else {
        // No cache found or expired.
        $fetch = TRUE;
      }
    }

    if ($fetch) {
      if ($obj = $this->createSubscriberObj($listId)) {
        $this->subscribers[$listId . $email] = [];
        $result = $obj->get($email);
        if ($result->was_successful()) {
          foreach ($result->response as $key => $value) {
            if ($key == 'CustomFields') {
              // Convert the custom fields object into a keyed array.
              $this->subscribers[$listId . $email][$key] = [];
              foreach ($value as $field) {
                // Check if the field has been set. If not, set the value.
                if (!isset($this->subscribers[$listId . $email][$key][$field->Key])) {
                  $this->subscribers[$listId . $email][$key][$field->Key] = $field->Value;
                }
                // If the field HAS been set, and there are additional values,
                // check if the field is NOT an array?
                elseif (!is_array($this->subscribers[$listId . $email][$key][$field->Key])) {
                  // If the field is not an array, assign an array to the field,
                  // containing the previous value of the field and this new
                  // value.
                  $this->subscribers[$listId . $email][$key][$field->Key] = [$this->subscribers[$listId . $email][$key][$field->Key], $field->Value];
                }
                // If the field is an array and there is an additional value,
                // append it to the field rather than overwriting the field
                // value.
                else {
                  $this->subscribers[$listId . $email][$key][$field->Key][] = $field->Value;
                }
              }
            }
            else {
              $this->subscribers[$listId . $email][$key] = $value;
            }
          }

          // Save the subscriber information in the cache.
          $this->cacheBackend->set('campaignmonitor_subscribers', $this->subscribers, $this->getCacheTimeout());
        }
        else {
          $this->addError('error', $result->response->Message, $result->http_status_code);
          return [];
        }
      }
      else {
        return FALSE;
      }
    }

    return $this->subscribers[$listId . $email];
  }

  /**
   * Check if user, identified by e-mail, is subscribed to a given list.
   *
   * @param string $listId
   *   The unique Campaign Monitor list ID.
   * @param string $email
   *   The user's e-mail address.
   *
   * @return bool
   *   TRUE if subscribed, FALSE if not.
   */
  public function isSubscribed($listId, $email) {
    $result = $this->getSubscriber($listId, $email);
    if (!empty($result)) {
      if ($result['State'] == 'Active') {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Remove subscribers from local cache.
   *
   * This forces the data to be fetched from Campaign
   * Monitor at the next request. This function should be used in
   * connection with updating subscriber information.
   *
   * @param mixed $listId
   *   The unique Campaign Monitor list ID.
   * @param mixed $email
   *   The e-mail address to be removed from cache.
   */
  public function removeSubscriberFromCache($listId, $email) {
    if (($cache = $this->cacheBackend->get('campaignmonitor_subscribers')) && !empty($cache->data)) {
      // Cache information found.
      $this->subscribers = $cache->data;
      if (isset($this->subscribers[$listId . $email])) {
        // Subscriber found in the cache, so remove it.
        unset($this->subscribers[$listId . $email]);
        $this->cacheBackend->set('campaignmonitor_subscribers', $this->subscribers, $this->getCacheTimeout());
      }
    }
  }

  /**
   * Subscribe a user to a given list, with information entered.
   *
   * If the user is already subscribed to the list, information
   * will be updated with the new values.
   *
   * @param string $listId
   *   The unique Campaign Monitor list ID.
   * @param string $email
   *   The e-mail address that identifies the user.
   * @param string $name
   *   Optionally the name of the user.
   * @param array $customFields
   *   Optionally some custom fields that were defined in Campaign Monitor.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise.
   */
  public function subscribe($listId, $email, $name = '', array $customFields = []) {

    if ($obj = $this->createSubscriberObj($listId)) {
      $result = $obj->add([
        'EmailAddress' => $email,
        'Name' => $name,
        'CustomFields' => $customFields,
        'ConsentToTrack' => 'yes',
        'Resubscribe' => TRUE,
      ]);

      if (!$result->was_successful()) {
        $this->addError($result->response->Message, $result->http_status_code);
        return FALSE;
      }
      $this->removeSubscriberFromCache($listId, $email);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Unsubscribe a given user, identified by e-mail address, from a given list.
   *
   * @param string $listId
   *   The unique Campaign Monitor list ID.
   * @param string $email
   *   The e-mail address that identifies the user.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise.
   */
  public function unsubscribe($listId, $email) {
    if ($obj = $this->createSubscriberObj($listId)) {
      $result = $obj->unsubscribe($email);
      if (!$result->was_successful()) {
        $this->addError($result->response->Message, $result->http_status_code);
        return FALSE;
      }
      $this->removeSubscriberFromCache($listId, $email);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Updates the subscriber e-mail address for a given list.
   *
   * @param array $listId
   *   The unique Campaign Monitor list ID.
   * @param string $oldEmail
   *   The old e-mail address.
   * @param string $email
   *   The new e-mail address.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise.
   */
  public function updateSubscriberEmail(array $listId, $oldEmail, $email) {
    if ($obj = $this->createSubscriberObj($listId)) {
      $result = $obj->update($oldEmail, [
        'EmailAddress' => $email,
        'Resubscribe' => TRUE,
      ]);
      if (!$result->was_successful()) {
        $this->addError($result->response->Message, $result->http_status_code);
        return FALSE;
      }

      // Remove the old e-mail address from the subscriber cache.
      $this->removeSubscriberFromCache($listId, $oldEmail);
      return TRUE;
    }
  }

  /**
   * Helper function to get a user's subscriptions.
   *
   * @param string $email
   *   The user mail id.
   * @param string $format
   *   Return value depends on format requested.
   *
   * @return array
   *   list names.
   */
  public function getUserSubscriptions($email, $format = 'checkboxes') {
    $lists = $this->getLists();

    // Build options for the form selector.
    $options = [];
    $default = [];

    foreach ($lists as $list_id => $list) {
      // Check if the list is selected to be shown.
      // $list_options = $this->campaignMonitorManager
      // ->getListSettings($list_id);.
      if ($this->isListEnabled($list_id)) {

        // Check if the user is subscribed to the current list.
        $default[$list_id] = 0;
        if ($this->isSubscribed($list_id, $email)) {
          $default[$list_id] = $list_id;
          $options[$list_id] = $list['name'];
        }
      }
    }

    switch ($format) {
      case 'checkboxes':
        return $default;

      default:
        return $options;
    }
  }

  /**
   * Clears all the caches used by this wrapper object.
   */
  public function clearCache() {
    $caches = ['cache.config', 'cache.data'];
    // Flush entity and render persistent caches.
    $this->moduleHandler->invokeAll('cache_flush');
    foreach (Cache::getBins() as $service_id => $cache_backend) {
      if (in_array($service_id, $caches)) {
        $cache_backend->deleteAll();
      }
    }
  }

}
