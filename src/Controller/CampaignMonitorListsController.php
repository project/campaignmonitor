<?php

namespace Drupal\campaignmonitor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\campaignmonitor\CampaignMonitorManager;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Campaign Monitor Lists controller.
 */
class CampaignMonitorListsController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The campaignmonitor manager.
   *
   * @var \Drupal\campaignmonitor\CampaignMonitorManager
   */
  protected $campaignMonitorManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * TaxonomyViewsIntegratorManager constructor.
   *
   * @param \Drupal\campaignmonitor\CampaignMonitorManager $campaignmonitor_manager
   *   The campaign monitor manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(CampaignMonitorManager $campaignmonitor_manager,
    ConfigFactoryInterface $config_factory) {
    $this->campaignMonitorManager = $campaignmonitor_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $campaignmonitor_manager = $container->get('campaignmonitor.manager');
    return new static(
      $campaignmonitor_manager,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function overview() {
    $content = [];

    $lists_admin_url = Url::fromUri('https://waxeye.createsend.com/subscribers/', ['attributes' => ['target' => '_blank']]);

    $lists_empty_message = $this->t("You don't have any lists configured in your
      Campaign Monitor account, (or you haven't configured your API key correctly on
      the Global Settings tab). Head over to @link and create some lists, then
      come back here and click 'Refresh lists from Campaign Monitor'",
      ['@link' => Link::fromTextAndUrl($this->t('Campaign Monitor'), $lists_admin_url)->toString()]);

    $content['lists_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('List ID'),
        $this->t('Subscribed'),
        $this->t('Operations'),
      ],
      '#empty' => $lists_empty_message,
    ];

    $cm_lists = $this->campaignMonitorManager->getLists();
    // $total_webhook_events = count(campaignmonitor_default_webhook_events());
    foreach ($cm_lists as $key => $cm_list) {

      $stats = $this->campaignMonitorManager->getListStats($key);

      $edit_link = Link::fromTextAndUrl($this->t('Edit'),
        Url::fromUri('internal:/admin/config/services/campaignmonitor/list/' . $key . '/edit'))->toString();

      $delete_link = Link::fromTextAndUrl($this->t('Delete'),
        Url::fromUri('internal:/admin/config/services/campaignmonitor/list/' . $key . '/delete'))->toString();

      $operations = [
        'Edit' => $edit_link,
        'Delete' => $delete_link,
      ];

      $list_options = $this->campaignMonitorManager->getListSettings($key);
      if (isset($list_options['status']['enabled']) && !$list_options['status']['enabled']) {
        // Add enable operation.
        $link = Link::fromTextAndUrl($this->t('Enable'),
          Url::fromUri('internal:/admin/config/services/campaignmonitor/list/' . $key . '/enable'))->toString();

        $operations['enable'] = $link;
      }
      else {
        // Add disable operation.
        $link = Link::fromTextAndUrl($this->t('Disable'),
          Url::fromUri('internal:/admin/config/services/campaignmonitor/list/' . $key . '/disable'))->toString();

        $operations['disable'] = $link;
      }

      /*$enabled_webhook_events =
      count(campaignmonitor_enabled_webhook_events($cm_list->id));
      $webhook_url =
      Url::fromRoute('campaignmonitor.webhook', ['list_id' => $cm_list->id]);
      $webhook_link = Link::fromTextAndUrl('update', $webhook_url);

      $webhook_status =
      $enabled_webhook_events . ' of ' . $total_webhook_events .
      ' enabled (' .  $webhook_link->toString() . ')';
      $list_url = Url::fromUri(
      'https://admin.campaignmonitor.com/lists/dashboard/overview?id=' .
      $cm_list->id,
      ['attributes' => ['target' => '_blank']]);*/

      $content['lists_table'][$key]['name'] = [
        '#markup' => $cm_list['name'],
      // '#type' => 'link',
      // '#url' => $list_url.
      ];
      $content['lists_table'][$key]['id'] = [
        '#markup' => $key,
      ];
      $content['lists_table'][$key]['stats'] = [
        '#markup' => $stats['TotalActiveSubscribers'] . ' / ' . $stats['TotalUnsubscribes'],
      ];
      $content['lists_table'][$key]['operations'] = [
        '#markup' => implode(' ', $operations),

      ];
    }

    $refresh_url = Url::fromRoute('campaignmonitor.refresh_lists', ['destination' => 'admin/config/services/campaignmonitor/lists']);

    $content['refresh'] = [
      '#type' => 'container',
    ];

    $content['refresh']['refresh_link'] = [
      '#title' => 'Refresh lists from CampaignMonitor',
      '#type' => 'link',
      '#url' => $refresh_url,
    ];

    return $content;
  }

  /**
   * Enable the list.
   */
  public function listEnable($list_id) {
    $this->verifyAccess();
    $this->listToggleEnable($list_id);
    $this->messenger()->addStatus('list enabled');
    return new RedirectResponse('/admin/config/services/campaignmonitor/lists');
  }

  /**
   * Disable the list.
   */
  public function listDisable($list_id) {
    $this->verifyAccess();
    $this->listToggleEnable($list_id);
    $this->messenger()->addStatus('list disabled');

    return new RedirectResponse('/admin/config/services/campaignmonitor/lists');
  }

  /**
   * Enables the toggling of list.
   */
  private function listToggleEnable($list_id) {
    // Get local list information and update enabled state.
    $list_options = $this->campaignMonitorManager->getListSettings($list_id);
    $enable = 0;
    if (isset($list_options['status']['enabled'])) {
      $enable = $list_options['status']['enabled'] == 1 ? 0 : 1;
    }
    $list_options['status']['enabled'] = $enable;
    $list_config = $this->configFactory->getEditable('campaignmonitor.settings.list');
    $list_key = $this->campaignMonitorManager->listKey($list_id);
    $list_config->set($list_key, $list_options)->save();

    // Clear blocks cache.
    // _block_rehash();
  }

  /**
   * Callback to clear config cache.
   */
  public function clearListCache() {
    $this->campaignMonitorManager->clearCache();
    $this->messenger()->addStatus('Campaign Monitor caches cleared');
    return new RedirectResponse('/admin/config/services/campaignmonitor/lists');
  }

  /**
   * Verify that a token is set for CSRF protection.
   */
  public function verifyAccess() {

    // If (!isset($_GET['token']) || !drupal_valid_token($_GET['token'])) {
    // drupal_not_found();
    // module_invoke_all('exit');
    // exit();
    // }
  }

}
