<?php

namespace Drupal\campaignmonitor_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\user\UserStorageInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\campaignmonitor_user\CampaignMonitorUserManager;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Controller routines for user routes.
 */
class CampaignMonitorUserController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Campaign monitor user manager.
   *
   * @var \Drupal\campaignmonitor_user\CampaignMonitorUserManager
   */
  protected $campaignMonitorUserManager;

  /**
   * Constructs a CampaignMonitorUserController object.
   */
  public function __construct(
    FormBuilder $form_builder,
    UserStorageInterface $user_storage,
    ConfigFactoryInterface $config_factory,
    LoggerInterface $logger,
    CampaignMonitorUserManager $campaignmonitor_user_manager
    ) {
    $this->formBuilder = $form_builder;
    $this->userStorage = $user_storage;
    $this->configFactory = $config_factory;
    $this->logger = $logger;
    $this->campaignMonitorUserManager = $campaignmonitor_user_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('config.factory'),
      $container->get('logger.factory')->get('user'),
      $container->get('campaignmonitor_user.manager')
    );
  }

  /**
   * View subscriptions.
   *
   * This controller assumes that it is only invoked for authenticated users.
   * This is enforced for the 'user.page' route with the '_user_is_logged_in'
   * requirement.
   */
  public function subscriptionPage() {
    // Get the user's current subscriptions.
    $config = $this->configFactory->get('campaignmonitor_user.settings');
    $config = $config->getRawData();
    return $this->formBuilder->getForm('\Drupal\campaignmonitor\Form\CampaignMonitorSubscribeForm', $config);
  }

}
