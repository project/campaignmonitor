<?php

namespace Drupal\campaignmonitor_user;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\campaignmonitor\CampaignMonitorManager;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Manager for Campaignmonitor subscriptions.
 */
class CampaignMonitorUserManager extends CampaignMonitorManager {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The module manager service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The campaignmonitor manager.
   *
   * @var \Drupal\campaignmonitor\CampaignMonitorManager
   */
  protected $campaignMonitorManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The loggers service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\campaignmonitor\CampaignMonitorManager $campaignmonitor_manager
   *   The campaign monitor manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The account interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      LoggerChannelFactoryInterface $logger_factory,
      CacheBackendInterface $cache_backend,
      LanguageManagerInterface $language_manager,
      ModuleHandlerInterface $module_handler,
      CampaignMonitorManager $campaignmonitor_manager,
      AccountProxyInterface $account,
      EntityTypeManagerInterface $entity_type_manager
    ) {
    parent::__construct($config_factory, $logger_factory, $cache_backend, $language_manager, $module_handler);
    $this->campaignMonitorManager = $campaignmonitor_manager;
    $this->account = $account;
    $this->entityTypeManager = $entity_type_manager;
  }

}
