<?php

namespace Drupal\campaignmonitor_user\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\campaignmonitor\CampaignMonitorSubscriptionManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure CM settings for this site.
 */
class CampaignMonitorUserAdminForm extends ConfigFormBase {

  /**
   * Subscription manager service.
   *
   * @var Drupal\campaignmonitor\CampaignMonitorSubscriptionManager
   */
  protected $campaignMonitorSubscriptionManager;

  /**
   * Class constructor.
   */
  public function __construct(
    CampaignMonitorSubscriptionManager $campaignmonitor_subscription_manager
  ) {

    $this->campaignMonitorSubscriptionManager = $campaignmonitor_subscription_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('campaignmonitor.subscription_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'campaignmonitor_user_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['campaignmonitor_user.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('campaignmonitor_user.settings');

    $form += $this->campaignMonitorSubscriptionManager->subscribeSettingsForm($config->getRawData());

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('campaignmonitor_user.settings');
    $config
      ->set('list', $form_state->getValue('list'))
      ->set('list_id', $form_state->getValue('list_id'))
      ->set('list_id_text', $form_state->getValue('list_id_text'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
