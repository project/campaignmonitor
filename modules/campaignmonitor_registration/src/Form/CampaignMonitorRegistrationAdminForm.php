<?php

namespace Drupal\campaignmonitor_registration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\campaignmonitor\CampaignMonitorSubscriptionManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure campaignmonitor settings for this site.
 */
class CampaignMonitorRegistrationAdminForm extends ConfigFormBase {

  /**
   * The campaign monitor subscription manager.
   *
   * @var Drupal\campaignmonitor\CampaignMonitorSubscriptionManager
   */
  protected $campaignMonitorSubscriptionManager;

  /**
   * Class constructor.
   */
  public function __construct(
    CampaignMonitorSubscriptionManager $campaignmonitor_subscription_manager
  ) {

    $this->campaignMonitorSubscriptionManager = $campaignmonitor_subscription_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('campaignmonitor.subscription_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'campaignmonitor_registration_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['campaignmonitor_registration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('campaignmonitor_registration.settings');

    $form['checkbox_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Registration text'),
      '#description' => $this->t('The text to use by the checkbox that reveals the newsletter lists'),
      '#default_value' => $config->get('checkbox_text'),
    ];

    $form += $this->campaignMonitorSubscriptionManager->subscribeSettingsForm($config->getRawData());

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('campaignmonitor_registration.settings');
    $config
      ->set('checkbox_text', $form_state->getValue('checkbox_text'))
      ->set('list', $form_state->getValue('list'))
      ->set('list_id', $form_state->getValue('list_id'))
      ->set('list_id_text', $form_state->getValue('list_id_text'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
